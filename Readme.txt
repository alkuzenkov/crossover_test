To run the project please do following steps:
1) Rename folder "Code" to "expos" and move it to the web server folder (e.g /var/www).
2) Create local domain 'crossover.lan' and make it look into 'public' folder of the project.
Example for ubuntu 16.04:
	A) In /etc/apache2/sites-available/000-default.conf write 
	<VirtualHost *:80>
		ServerName crossover.lan

		ServerAdmin webmaster@localhost
		DocumentRoot /var/www/expos/public

		ErrorLog ${APACHE_LOG_DIR}/error.log
		CustomLog ${APACHE_LOG_DIR}/access.log combined
	</VirtualHost>
	B) In /etc/hosts add 'crossover.lan' to localhost.
	C) Reboot apache by running 'sudo service apache2 restart'.
3) To create Database run following SQL 'CREATE SCHEMA `expos` DEFAULT CHARACTER SET utf8;'. DB configs is in .env file.
4) Run migrations from root of the project to create tables 'php artisan migrate'.
5) To run cron tasks create task * * * * * www-data php /var/www/expos/artisan schedule:run >> /dev/null 2>&1. Instead www-data could be any user that runs with web server.
6) Done! Project is ready to check.

Thank you!
