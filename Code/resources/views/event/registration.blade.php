<?php
/**
 * @var Illuminate\Support\MessageBag $errors
 * @var \App\Stand $stand
 */
?>
@extends('layouts.main')
@section('title', 'Company Registration')
@section('body')
    <ng-controller ng-controller="RegistrationCtrl">
        <div class="row m-b-25">
            <h2>@yield('title')</h2>
            <a href="{{ url('') }}" class="btn btn-default"><< Back</a>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Order details</div>
            <div class="panel-body">
                <div class="col-xs-3 text-right text-strong">Event:</div>
                <div class="col-xs-9">{{ $stand->event->name }}</div>
                <div class="col-xs-3 text-right text-strong">Location:</div>
                <div class="col-xs-9">{{ $stand->event->location }}</div>
                <div class="col-xs-3 text-right text-strong">Start time:</div>
                <div class="col-xs-9">{{ $stand->event->start_at->toDayDateTimeString() }}</div>
                <div class="col-xs-3 text-right text-strong">End time:</div>
                <div class="col-xs-9">{{ $stand->event->end_at->toDayDateTimeString() }}</div>
                <div class="col-xs-3 text-right text-strong">Stand number:</div>
                <div class="col-xs-9">{{ $stand->number }}</div>
                <div class="col-xs-3 text-right text-strong">Description:</div>
                <div class="col-xs-9">{{ $stand->description }}</div>
                <div class="col-xs-3 text-right text-strong">Photo:</div>
                <div class="col-xs-9"><img src="{{ url('/' . $stand->image) }}"></div>
                <div class="col-xs-12"><strong>Total price:</strong> ${{ $stand->price }}</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Registration form</div>
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data" action="">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <textarea name="contacts" class="form-control" placeholder="Contact details...">{{ old('details') }}</textarea>
                        <small class="text-danger">{{ $errors->first('details') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="documents">Marketing documents</label>
                        <input type="file" name="documents[]" id="documents" multiple
                               accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/jpeg, image/png"
                        >
                        <small class="text-danger">{{ $errors->first('documents') }}</small>
                    </div>
                    <div class="form-group">
                        <input type="email" name="admin_email" class="form-control" required maxlength="255"
                               placeholder="Admin email" value="{{ old('admin_email') }}"
                        >
                        <small class="text-danger">{{ $errors->first('admin_email') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="logo">Logo</label>
                        <input type="file" name="logo" id="logo" accept="image/jpeg, image/png"
                               onchange="angular.element(this).scope().loadPreview(this)"
                        >
                        <small class="text-danger">{{ $errors->first('logo') }}</small>
                        <div class="p-t-25"><img src="@{{preview_src}}"></div>
                    </div>
                    <button type="submit" class="btn btn-success">Confirm Reservation</button>
                    <a href="{{ url('') }}" class="btn btn-link">Cancel</a>
                </form>
            </div>
        </div>
    </ng-controller>
    <script>
        angular.module('MainApp').controller('RegistrationCtrl', function ($scope) {
            $scope.preview_src = '';
            $scope.loadPreview = function (input) {
                if (!input.files || !input.files[0]) {
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.preview_src = e.target.result;
                    });
                };
                reader.readAsDataURL(input.files[0]);
                return true;
            }
        })
    </script>
@endsection