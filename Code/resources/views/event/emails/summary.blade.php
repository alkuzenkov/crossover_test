<?php
/**
 * @var \App\Stand $stand
 * @var \App\Event $event
 */
?>
Event {{ $event->name }} is ended. You had {{ rand(0, 25000) }} visitors at the stand #{{ $stand->id }}.
