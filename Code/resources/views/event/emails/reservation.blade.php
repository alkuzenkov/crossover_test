<?php
/**
 * @var \App\Stand $stand
 */
?>
Stand # {{ $stand->number }} was reserved at {{ \Carbon\Carbon::create()->toDayDateTimeString() }} for event "{{ $stand->event->name }}".<br>
Event: {{ $stand->event->name }}<br>
Location: {{ $stand->event->location }}<br>
Start time: {{ $stand->event->start_at->toDayDateTimeString() }}<br>
End time: {{ $stand->event->end_at->toDayDateTimeString() }}<br>
Stand number: {{ $stand->number }}<br>
Description: {{ $stand->description }}<br>
Price: ${{ $stand->price }}
