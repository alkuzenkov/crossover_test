<!DOCTYPE html>
<html lang="ru" ng-app="MainApp">
<head>
    <title>@yield('title') | Expositions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('vendor/chosen/chosen.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendor/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/chosen/chosen.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/angular/angular.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/main_app.js') }}" type="text/javascript"></script>
</head>
<body>
<nav class="navbar-default bg-warning">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('') }}">Expositions</a>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row content">
        @yield('body')
    </div>
</div>
</body>
</html>