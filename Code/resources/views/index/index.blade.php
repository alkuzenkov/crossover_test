<?php
/**
 * @var string $events
 */
?>
@extends('layouts.main')
@section('title', 'Events Map')
@section('body')
    <ng-controlelr ng-controller="EventsMapCtrl">
        <div class="row m-b-25">
            <h2>@yield('title')</h2>
        </div>
        <ng-show ng-show="show_global_map">
            <div id="events_map" class="row m-b-15"></div>
            <div class="row m-b-15">
                <button type="button" class="btn btn-success btn-block" ng-disabled="!book_enabled" ng-click="showEventMap()">Book your place</button>
            </div>
        </ng-show>
        <div id="event_block" class="row m-b-15" ng-show="!show_global_map">
            <div class="col-xs-6"><h3>Map for event "@{{ event.name }}"</h3></div>
            <div class="col-xs-6 text-right">
                <button type="button" ng-click="showGlobalMap()" class="btn btn-default">
                    << Back to global map
                </button>
            </div>
            <div class="col-xs-12" ng-if="booked_stand">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Success!</strong>
                    You have successfully booked the stand #@{{ booked_stand.number }} at the price of $@{{ booked_stand.price }}.
                    Have a nice time on "@{{ event.name }}".
                </div>
            </div>
            <ng-include src="url"></ng-include>
        </div>
        <div class="mouse-popover popover right">
            <div class="arrow"></div>
            <h3 class="popover-title">@{{ popover.title }}</h3>
            <div class="popover-content"><p>@{{ popover.content }}</p></div>
        </div>
        <div id="stand_modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Stand #@{{ stand.number }} for "@{{ event.name }}"</h4>
                    </div>
                    <div class="modal-body">
                        <ng-show ng-show="!stand.company_id">
                            <p><strong>Event</strong>: @{{ event.name }}</p>
                            <p><strong>Event location</strong>: @{{ event.location }}</p>
                            <p><strong>Stand #</strong>: @{{ stand.number }}</p>
                            <p><strong>Description</strong>: @{{ stand.description }}</p>
                            <p><strong>Price</strong>: $@{{ stand.price }}</p>
                            <p><strong>Photo</strong>: <img src="@{{ stand.image }}"></p>
                        </ng-show>
                        <ng-show ng-show="stand.company_id">
                            <p ng-if="stand.company.logo"><img src="/companies/@{{ stand.company_id }}/@{{ stand.company.logo }}"></p>
                            <p ng-repeat="document in stand.company.documents">
                                <a href="/companies/@{{ $parent.stand.company_id }}/docs/@{{ document }}" download>
                                    @{{ document }}
                                </a>
                            </p>
                            <p ng-if="stand.company.contacts"><strong>Contacts</strong>: @{{ stand.company.contacts }}</p>
                        </ng-show>
                    </div>
                    <div class="modal-footer" ng-show="!stand.company_id">
                        <a href="/registration/@{{ stand.id }}" class="btn btn-success">Reserve</a>
                    </div>
                </div>
            </div>
        </div>
    </ng-controlelr>
    <script>
        angular.module('MainApp').controller('EventsMapCtrl', function ($scope, $http) {
            // Init functions
            $scope.showEventMap = function () {
                $http.get('events/' + $scope.event.id + '?_token={{ csrf_token() }}').then(function (response) {
                    $scope.stands = angular.fromJson(response.data.stands);
                    $scope.url = 'templates/event_map.html';
                    $scope.show_global_map = false;
                });
            };
            $scope.showGlobalMap = function () {
                $scope.show_global_map = true;
                if ($scope.booked_stand) {
                    $scope.booked_stand = null;
                    initGoogleMaps();
                }
            };
            var initGoogleMaps = function () {
                var events_map = new google.maps.Map(document.getElementById('events_map'), {
                    center: {lat: 59.932551, lng: 30.233934},
                    scrollwheel: true,
                    zoom: 13
                });
                var infowindow = new google.maps.InfoWindow();
                angular.forEach($scope.events, function (event) {
                    var marker = new google.maps.Marker({
                        map: events_map,
                        position: {lat: event.latitude, lng: event.longitude},
                        title: event.name
                    });
                    marker.addListener('click', function() {
                        infowindow.setContent('<div>'+
                                '<h1>' + event.name + '</h1>'+
                                '<div>'+
                                '<p><b>Location</b>: ' + event.location + '</p>'+
                                '<p><b>Start time</b>: ' + event.start_at + '</p>'+
                                '<p><b>End time</b>: ' + event.end_at + '</p>'+
                                '</div>'+
                                '</div>');
                        infowindow.open(events_map, this);
                        $scope.$apply(function () {
                            $scope.event = event;
                            $scope.book_enabled = true;
                        });
                    });
                });
                google.maps.event.addListener(infowindow,'closeclick',function() {
                    $scope.$apply(function () {
                        $scope.event = {};
                        $scope.book_enabled = false;
                    });
                });
            };

            // Init variables
            $scope.events = {!! $events !!};
            $scope.popover = {title: '', content: ''};
            $scope.book_enabled = false;
            $scope.url = '';
            $scope.event = {};
            $scope.stand = {};
            $scope.booked_stand = {!! session('booked_stand') ? session('booked_stand') : 'null' !!};
            $scope.show_global_map = !$scope.booked_stand;
            if ($scope.show_global_map) {
                initGoogleMaps();
            } else {
                $scope.event = $scope.events[$scope.booked_stand.event_id];
                $scope.showEventMap();
            }

            // Init jQuery event listeners
            var mouse_popover = $('.mouse-popover');
            var stand_modal = $('#stand_modal');
            $('body').on('mousemove', '.mall_shop_item', function (event) {
                var stand = $scope.stands[$(this).attr('id').replace('stand_', '')];
                if (!stand) {
                    return false;
                }
                $scope.$apply(function () {
                    $scope.popover.title = 'Stand #' + stand.number;
                    if (stand.company_id) {
                        $scope.popover.content = 'This stand is reserved.';
                    } else {
                        $scope.popover.content = 'This stand is free for reservation! Current price is $' + stand.price + '.';
                    }
                    $scope.popover.content += ' Click for more details.'
                });
                mouse_popover.css('left', Math.max(0, event.pageX + 25) + 'px');
                mouse_popover.css('top', (event.pageY - 50) + 'px');
                mouse_popover.show();
                return true;
            }).on('mouseleave', '.mall_shop_item', function () {
                mouse_popover.hide();
            }).on('click', '.mall_shop_item', function () {
                var stand_id = $(this).attr('id').replace('stand_', '');
                $scope.$apply(function () {
                    $scope.stand = $scope.stands[stand_id];
                });
                stand_modal.modal();
            });
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7zISJUbCaszGPYiIYn0rDgyWAKkK4uPc"></script>
@endsection