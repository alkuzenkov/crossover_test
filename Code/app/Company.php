<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    /** @var boolean */
    public $timestamps = false;

    /** @var string[] */
    protected $fillable = ['contacts', 'admin_email', 'logo', 'documents'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stands()
    {
        return $this->hasOne(Stand::class);
    }
}
