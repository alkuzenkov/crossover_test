<?php

namespace App\Console\Commands;

use App\Event;
use Illuminate\Console\Command;
use Mail;
use Carbon\Carbon;

class EventSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail about stand visits.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = Event::with('stands')
            ->where('end_at', '<', Carbon::create()->toDateTimeString())
            ->where('is_summary_sent', 0)
            ->get();
        foreach ($events as $event) {
            foreach ($event->stands as $stand) {
                if (!$stand->company_id) {
                    continue;
                }
                Mail::send('event.emails.summary', [
                    'stand' => $stand, 'event' => $event
                ], function ($mail) use ($stand) {
                    $mail->to($stand->company->admin_email, $stand->company->admin_email);
                    $mail->subject('Summary for stand #' . $stand->number);
                });
                $this->info('Mail to ' . $stand->company->admin_email . ' sent.');
            }
            $event->is_summary_sent = 1;
            $event->save();
        }
    }
}
