<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    /** @var boolean */
    public $timestamps = false;

    /** @var string[] */
    protected $dates = ['start_at', 'end_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stands()
    {
        return $this->hasMany(Stand::class);
    }

}
