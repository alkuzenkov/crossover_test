<?php
Route::get('', 'IndexController@getIndex');

Route::get('events/{id}', 'EventsController@getEvent');
Route::get('registration/{stand_id}', 'EventsController@getRegistration');
Route::post('registration/{stand_id}', 'EventsController@postRegistration');
