<?php

namespace App\Http\Controllers;

use App\Event;
use Carbon\Carbon;
use View;

class IndexController extends Controller
{

    /**
     * @return View
     */
    public function getIndex()
    {
        return view('index.index', [
            'events' => Event::where('end_at', '>', Carbon::create()->toDateTimeString())->get()->keyBy('id')->toJson()
        ]);
    }

}