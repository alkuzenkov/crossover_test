<?php

namespace App\Http\Controllers;

use App\Company;
use App\Event;
use App\Stand;
use Carbon\Carbon;
use Request;
use Validator;
use File;
use Mail;

use App\Http\Requests;

class EventsController extends Controller
{

    /**
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEvent($id)
    {
        $stands = Event::find($id)->stands->keyBy('number');
        foreach ($stands as $stand) {
            if ($stand->company) {
                $stand->company->documents = explode('!!##%', $stand->company->documents);
            }
        }
        return response()->json(['stands' => $stands->toJson()]);
    }

    /**
     * @param integer $stand_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegistration($stand_id)
    {
        return view('event.registration', ['stand' => $this->getAndValidateStand($stand_id)]);
    }

    /**
     * @param integer $stand_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postRegistration($stand_id)
    {
        $stand = $this->getAndValidateStand($stand_id);
        Request::flash();
        $form = Request::all();
        $validator = Validator::make($form, [
            'contacts' => 'min:2|max:255',
            'admin_email' => 'required|email|max:255',
            'logo' => 'mimes:jpeg,png|max:250',
            'documents.*' => 'mimetypes:application/msword,application/vnd.ms-excel,application/vnd.ms-powerpoint,text/plain,application/pdf,image/jpeg,image/png',
        ]);
        if ($validator->fails()) {
            return redirect('/registration/' . $stand->id)->withErrors($validator);
        }
        $documents = [];
        if (!empty($form['documents'])) {
            foreach ($form['documents'] as $document) {
                if ($document) {
                    $documents[] = $document->getClientOriginalName();
                }
            }
        }
        $logo = !empty($form['logo']) ? $form['logo']->hashName() : '';
        $company = Company::create([
            'contacts' => $form['contacts'],
            'admin_email' => $form['admin_email'],
            'logo' => $logo,
            'documents' => implode('!!##%', $documents)
        ]);
        $stand->company_id = $company->id;
        $stand->save();
        $company_folder = base_path('public/companies/' . $company->id);
        if (!empty($form['logo'])) {
            File::makeDirectory($company_folder, 0775, true);
            $form['logo']->move($company_folder, $logo);
        }
        if (!empty($form['documents'])) {
            $docs_folder = $company_folder . '/docs';
            File::makeDirectory($docs_folder, 0775, true);
            foreach ($form['documents'] as $document) {
                if ($document) {
                    $document->move($docs_folder, $document->getClientOriginalName());
                }
            }
        }
        Mail::send('event.emails.reservation', ['stand' => $stand], function ($mail) use ($company, $stand) {
            $mail->to($company->admin_email, $company->admin_email);
            $mail->subject('Reservation of stand #' . $stand->number);
        });
        return redirect('')->with('booked_stand', $stand->toJson());
    }

    /**
     * @param integer $stand_id
     * @return Stand
     */
    private function getAndValidateStand($stand_id)
    {
        $stand = Stand::find($stand_id);
        if (!$stand || $stand->company_id || $stand->event->end_at->lt(Carbon::create())) {
            abort(404);
        }
        return $stand;
    }

}
