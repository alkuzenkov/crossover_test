<?php

namespace App\Http\Controllers\Auth;

use Request;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;

class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new authentication controller instance.
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), array(
            'except' => array('logout', 'getLogout', 'getSuccessfulRegistration')
        ));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, array(
            'email' => 'required|email|max:255|unique:users',
            'login' => 'required|alpha_dash|min:2|max:50|unique:users'
        ));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create(array('login' => $data['login'], 'email' => $data['email']));
    }

    /**

     * @return string
     */
    public function loginUsername()
    {
        return 'email';
    }

    public function getSuccessfulRegistration()
    {
        return view('auth.successful_registration');
    }

    public function getVerification($login = null, $hash = null)
    {
        if (!$this->isValidLoginAndHash($login, $hash)) {
            abort(404);
        }
        return view('auth.verification');
    }

    /**
     * @param string $login
     * @param string $hash
     * @return boolean
     */
    private function isValidLoginAndHash($login, $hash)
    {
        if (!$login || !$hash) {
            return false;
        }
        /** @var User $user */
        $user = User::where('login', '=', $login)->first();
        return $user && $hash == $user->getVerificationHash();
    }

    public function postVerification($login = null, $hash = null)
    {
        Request::flash();
        $form = Request::all();
        $validator = Validator::make($form, array('password' => 'required|min:6|max:50|confirmed'));
        if ($validator->fails() || !$this->isValidLoginAndHash($login, $hash)) {
            return redirect('auth/verification/' . $login . '/' . $hash)->withErrors($validator);
        }
        /** @var User $user */
        $user = User::where('login', '=', $login)->first();
        $user->password = bcrypt($form['password']);
        $user->remember_token = md5('4qzAwP' . bcrypt($form['password']));
        $user->save();
        Auth::guard($this->getGuard())->login($user);
        return redirect('news/my');
    }

}
