<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Event;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location');
            $table->double('latitude', 8, 6);
            $table->double('longitude', 9, 6);
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->boolean('is_summary_sent')->default(0);
        });

        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->text('contacts');
            $table->string('documents');
            $table->string('admin_email');
            $table->string('logo');
        });

        Schema::create('stands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->integer('number')->unsigned();
            $table->integer('company_id')->unsigned()->nullable();
            $table->integer('price')->unsigned();
            $table->text('description');
            $table->string('image');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('set null')->onUpdate('cascade');
        });

        # Let's imagine that some managers fill these events.
        DB::table('events')->insert([
            [
                'name' => 'GameExpo',
                'location' => '196140, Russia, St.Petersburg, Peterburgskoye sh. 64/1',
                'latitude' => 59.932551,
                'longitude' => 30.233934,
                'start_at' => '2017-03-11 12:00',
                'end_at' => '2017-03-13 18:00'
            ],
            [
                'name' => 'Highload',
                'location' => '21th ling. Vasilyevsky Island, 16 Building 7, St. Petersburg, Russia, 19910',
                'latitude' => 59.936614,
                'longitude' => 30.259445,
                'start_at' => '2017-02-15 12:00',
                'end_at' => '2017-02-17 18:00'
            ],
            [
                'name' => 'Business Talks',
                'location' => 'Bolshaya Morskaya Str., 35 St. Petersburg, Russia 1900001',
                'latitude' => 59.933251,
                'longitude' => 30.310324,
                'start_at' => '2017-02-19 12:00',
                'end_at' => '2017-02-21 18:00'
            ],
        ]);
        foreach (Event::all() as $event) {
            $table = [];
            # 92 is count of stands on each event.
            for ($i = 1; $i <= 92; $i++) {
                $table[] = [
                    'number' => $i,
                    'event_id' => $event->id,
                    'company_id' => null,
                    'price' => rand(25000, 50000),
                    'description' => 'Nice stand #' . $i,
                    'image' => 'images/stands/empty_one.jpg'
                ];
            }
            DB::table('stands')->insert($table);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
        Schema::drop('companies');
        Schema::drop('stands');
    }
}
